/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.Test;

import br.com.senac.exercicio10.Aluno;
import br.com.senac.exercicio10.NotasAlunos;
import java.awt.AWTEventMulticaster;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class TesteAlunoComNotas {
    
    public TesteAlunoComNotas() {
    }
    @Test
    public void AlunoComNotaAcimaDe7() {
        
        List<Aluno> alunos = new ArrayList<>();
        Aluno tiago = new Aluno("tiago", 4);
        Aluno bruce = new Aluno("bruce", 2);
        Aluno bolsomito = new Aluno("bolsomito", 3);
        Aluno dilma = new Aluno("Dilma", 1);
        Aluno ciro = new Aluno("ciro", 2);
        
        alunos.add(tiago);
        alunos.add(bruce);
        alunos.add(bolsomito);
        alunos.add(dilma);
        alunos.add(ciro);
        
        Aluno alunoComNotaAcimade7  = NotasAlunos.getListarAlunos(alunos);
        assertNotEquals(bruce, alunoComNotaAcimade7 );
        
        
        
    }

   
}
