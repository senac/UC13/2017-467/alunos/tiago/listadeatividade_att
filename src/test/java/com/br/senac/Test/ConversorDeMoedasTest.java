/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.Test;

import br.com.senac.exercicio3.ConversorDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class ConversorDeMoedasTest {
    
    public ConversorDeMoedasTest() {
        
        }
    @Test
    public void deveConverter10ReaisPara2E45DollarAmericano(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.converterParaDollarAmericano(real);
        assertEquals(2.45, resultado , 0.01);
    }
    
    @Test
    public void deveConverter10ReaisPara3E38DollarAustraliano(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double real = 10 ; 
        
        double resultado  = conversorDeMoedas.converterParaDollarAustraliano(real);
        assertEquals(3.38, resultado , 0.01);
    }
}
