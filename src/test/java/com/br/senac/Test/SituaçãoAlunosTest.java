/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.Test;

import br.com.senac.exercicio6.AlunosAprovados;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class SituaçãoAlunosTest {

    public SituaçãoAlunosTest() {
    }
   
    @Test
    public void AlunosDeveSerAprovado() {

        int numero = 9;
        boolean resultado = AlunosAprovados.isAlunoAprovado(numero);
        assertTrue(resultado);

    }

    /*@Test
    public void AlunosDeveFicaRecuperação() {

        int numero = 5;
        boolean resultado = AlunosAprovados.isAlunoRecuperação(numero);
        assertFalse(resultado);

    }*/
@Test
    public void AlunosDeveFicaReprovados() {

        int numero = 2;
        boolean resultado = AlunosAprovados.isAlunoReprovados(numero);
        assertTrue(resultado);
    }
}
