/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.Test;

import br.com.senac.exercicio1.AttPessoas;
import br.com.senac.exercicio1.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class TestePessoa {

    public TestePessoa() {

    }

    @Test
    public void joaoDeveSerMaisNovo() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("joao", 10);
        Pessoa miguel = new Pessoa("Miguel", 15);
        Pessoa malaquias = new Pessoa("Malaquias", 20);

        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);

        Pessoa pessoaMaisNova = AttPessoas.getPessoaMaisNova(lista);
        assertNotEquals(joao, pessoaMaisNova);

    }
    
     @Test
    public void MiguelDeveSerMaisVelho() {

        List<Pessoa> lista = new ArrayList<>();
        Pessoa joao = new Pessoa("joao", 10);
        Pessoa miguel = new Pessoa("Miguel", 25);
        Pessoa malaquias = new Pessoa("Malaquias", 20);

        lista.add(joao);
        lista.add(miguel);
        lista.add(malaquias);

        Pessoa pessoaMaisVelha = AttPessoas.getPessoaMaisVelha(lista);
         assertNotEquals(miguel, pessoaMaisVelha);
}
}
