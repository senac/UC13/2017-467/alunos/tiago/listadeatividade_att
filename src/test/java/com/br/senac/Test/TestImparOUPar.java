/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.Test;

import br.com.senac.exercicio5.ImparOuPar;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class TestImparOUPar {
    
    public TestImparOUPar() {
    }
    @Test
    public void numerodeveserPar() {
        
        int numero = 4;
        boolean resultado = ImparOuPar.isPar(numero);
        assertTrue(resultado);
        
        
        //SEGUNDO JEITO
        //ImparOuPar positivo = new ImparOuPar(15);
        //assertEquals(positivo.ÉPositivo());
}
    
    @Test
    public void numerodeveserPosi() {
        
        int numero = 2000;
        boolean resultado = ImparOuPar.isPositivo(numero);
        assertTrue(resultado);
        
    
}
}
