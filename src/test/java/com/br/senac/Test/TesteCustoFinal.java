/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.Test;

import com.br.senac.exercicio2.CalculoPrecoCarro;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class TesteCustoFinal {
    
    public TesteCustoFinal() {
    }
    @Test
    public void CalcularPreçoFinal() {
        
        CalculoPrecoCarro calculadora = new CalculoPrecoCarro();
        
        double valorCusto = 10000;
        double resultado = calculadora.calcular(valorCusto);
        
        assertEquals(17300, resultado,0.01);
        
        
     }

}
