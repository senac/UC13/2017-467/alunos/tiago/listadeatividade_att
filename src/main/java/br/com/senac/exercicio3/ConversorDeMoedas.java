/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exercicio3;

/**
 *
 * @author sala304b
 */
public class ConversorDeMoedas {
    
    private  final double COTACAO_DOLLAR_US = 4.08 ; 
    private  final double COTACAO_DOLLAR_AU = 2.96 ; 
    private  final double COTACAO_EURO = 4.8 ; 
    
    
    
    public double converterParaDollarAmericano(double valor){
        return  valor / COTACAO_DOLLAR_US; 
    }
    
     public double converterParaDollarAustraliano(double valor){
        return valor/COTACAO_DOLLAR_AU;
    }
     
      public double converterParaEuro(double valor){
        return valor/COTACAO_EURO;
    }

    
}
