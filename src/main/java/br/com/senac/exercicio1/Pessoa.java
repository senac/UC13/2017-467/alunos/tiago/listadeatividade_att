/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exercicio1;

/**
 *
 * @author sala304b
 */
 public class Pessoa {
    
    private String Nome;
    private int Idade;
    
     public Pessoa() {
         
     }
       public Pessoa(String Nome, int Idade) {
        this.Nome = Nome;
        this.Idade = Idade;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public int getIdade() {
        return Idade;
    }

    public void setIdade(int Idade) {
        this.Idade = Idade;
    }

  
    
    
}
