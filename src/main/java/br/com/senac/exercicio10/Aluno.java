/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exercicio10;

/**
 *
 * @author sala304b
 */
  public class Aluno {
      
      private String aluno;
      private int nota;

    public Aluno(String aluno, int nota) {
        this.aluno = aluno;
        this.nota = nota;
    }

    public String getAluno() {
        return aluno;
    }

    public void setAluno(String aluno) {
        this.aluno = aluno;
    }

    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
    
}
