/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.br.senac.exercicio2;

/**
 *
 * @author sala304b
 */
public class CalculoPrecoCarro {

    private final double porcentagem = 0.28;
    private final double imposto = 0.45;

    public double calcular(double custo) {

        double Imposto = custo * imposto;
        double Porce = custo * porcentagem;
        double Precodefabrica = custo + Porce + Imposto;

        return Precodefabrica;

    }

}
